# Using Luke to inspect the index inside a docker volume

## What

Luke is a GUI tool to inspect lucene indices. Lucene is used by Elasticsearch and Solr.

## Why

This can be useful if you want to see what tokens are in the actual index.
However, if these indices are inside a docker volume it is hard to open them with Luke.

## How

Basically, you run Luke in a container (with the docker volume attached) and send the GUI to your own machine.

### OS X

You need two things: `socat` to open the connection between docker and X and `xQuartz` to have X to which the container can connect

```shell
brew install socat
socat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\"$DISPLAY\"
```

```shell
brew cask install xquartz
open -a Xquartz
```

In xQuartz

```shell
IP_ADDRESS=$(ifconfig en0 | grep "inet " | cut -d\  -f2) # grab your ip address
DOCKER_VOLUME=docker_volume_name # set name of the docker volume with lucene/solr/elastic data
docker run -it -e DISPLAY=$IP_ADDRESS:0 -v $DOCKER_VOLUME:/data registry.gitlab.com/roelandvanbatenburg/luke/luke-5.3.0 /luke/luke.sh
```

I had to go to Window->Close to be able to use the program.

When Luke has started open the index (`/data/elasticsearch-default/nodes/0/indices/<index>/0/index` for elasticsearch data)

### Linux

Probably much of the same, but less. Please let me know.

## Resources used

- https://github.com/DmitryKey/luke
- https://cntnr.io/running-guis-with-docker-on-mac-os-x-a14df6a76efc
- https://medium.com/@learnwell/how-to-dockerize-a-java-gui-application-bce560abf62a