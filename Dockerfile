ARG MAVEN_VERSION=3-jdk-8
FROM maven:$MAVEN_VERSION
ARG LUKE_VERSION=5.3.0
WORKDIR /

RUN git clone https://github.com/DmitryKey/luke.git
WORKDIR /luke
RUN git checkout tags/luke-$LUKE_VERSION

RUN mvn -B install